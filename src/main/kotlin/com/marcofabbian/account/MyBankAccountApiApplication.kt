package com.marcofabbian.account

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class MyBankAccountApiApplication

fun main(args: Array<String>) {
	runApplication<MyBankAccountApiApplication>(*args)
}
